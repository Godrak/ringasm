/*
 * mesh_assembly.h
 *
 *  Created on: 27 Nov 2021
 *      Author: pavel
 */

#ifndef SRC_MESH_ASSEMBLY_H_
#define SRC_MESH_ASSEMBLY_H_


#include "miscellaneous.h"
#include "ring_assembly.h"

#include <glm/glm.hpp>

#include <vector>
#include <algorithm>
#include <map>
#include <string>

namespace ringasm{

std::vector<vec3> computeNormals(const std::vector<vec3>& mesh) {
	auto result = std::vector<vec3> {};
	 for (int t = 0; t < mesh.size(); t+=3) {
		 auto normal = computeTriangleNormal(mesh[t], mesh[t+1], mesh[t+2]);
		 result.push_back(normal);
		 result.push_back(normal);
		 result.push_back(normal);
	 }
	 return result;
}


// WARNING: assumption is, that the ap one is facing the ap two
std::vector<vec3> connectRings(const std::vector<vec3>& ringOne,const std::vector<vec3>& ringTwo) {
	auto middleOne = centerOfMass(ringOne);
	auto middleTwo = centerOfMass(ringTwo);
	glm::vec3 direction = middleTwo - middleOne;
	if (glm::length(direction) < 0.001) {
		direction = glm::cross(ringOne[0], middleOne);
	}

	int localIndexOne = 0;
	auto chosenStartOne = ringOne[localIndexOne];

	int localIndexTwo = 0;
	auto distance = distanceToLine(chosenStartOne, ringTwo[localIndexTwo], direction);
	for (int i = 0; i < ringTwo.size(); ++i) {
		auto dis = distanceToLine(chosenStartOne, ringTwo[i], direction);
		if (dis < distance) {
			localIndexTwo = i;
			distance = dis;
		}
	}

	size_t stepsInOne = 0;
	size_t stepsInTwo = 0;
	std::vector<vec3> result;
	while (stepsInOne + stepsInTwo < ringOne.size() + ringTwo.size()) {
		auto v1 = ringOne[indexize(localIndexOne, ringOne.size())];
		auto v2 = ringTwo[indexize(localIndexTwo, ringTwo.size())];
		bool nextInOne;
		if (stepsInOne >= ringOne.size()) {
			nextInOne = false;
		} else if (stepsInTwo >= ringTwo.size()) {
			nextInOne = true;
		} else {
			nextInOne = stepsInOne / float(ringOne.size()) < stepsInTwo / float(ringTwo.size());
		}
		if (nextInOne) {
			result.push_back(ringOne[indexize(localIndexOne, ringOne.size())]);
			result.push_back(ringOne[indexize(localIndexOne + 1, ringOne.size())]);
			result.push_back(ringTwo[indexize(localIndexTwo, ringTwo.size())]);
			stepsInOne++;
			localIndexOne++;
		} else {
			result.push_back(ringTwo[indexize(localIndexTwo + 1, ringTwo.size())]);
			result.push_back(ringTwo[indexize(localIndexTwo, ringTwo.size())]);
			result.push_back(ringOne[indexize(localIndexOne, ringOne.size())]);
			stepsInTwo++;
			localIndexTwo++;
		}
	}
	return result;
}

bool chooseNextInOne(const vec3& v1, const vec3& v2, const vec3& one, const vec3& two) {

	auto a1 = computeArea(glm::distance(v1, v2), glm::distance(v1, one), glm::distance(v2, one));
	auto a2 = computeArea(glm::distance(v1, v2), glm::distance(v1, two), glm::distance(v2, two));
	return a1 > a2;

//	auto r1 = computeCircumcircleRadius(glm::distance(v1, v2), glm::distance(v1, one.position),
//			glm::distance(v2, one.position));
//	auto r2 = computeCircumcircleRadius(glm::distance(v1, v2), glm::distance(v1, two.position),
//			glm::distance(v2, two.position));
//	return r1 <= r2;
}

std::vector<vec3> closeRing(const std::vector<vec3>& ring, bool front) {
	int localIndexOne = 0;
	int localIndexTwo = 1;
	std::vector<vec3> result;
	for (int edge = 0; edge < ring.size() - 2; ++edge) {
		auto v1 = ring[localIndexOne];
		auto v2 = ring[localIndexTwo];
		auto nextInOne = chooseNextInOne(v1, v2, prev(ring, localIndexOne), next(ring, localIndexTwo));
		if (nextInOne) {
			result.push_back(ring[indexize(localIndexOne - 1, ring.size())]);
			result.push_back(ring[indexize(localIndexOne, ring.size())]);
			result.push_back(ring[indexize(localIndexTwo, ring.size())]);
			localIndexOne = indexize(localIndexOne - 1, ring.size());
		} else {
			result.push_back(ring[indexize(localIndexOne, ring.size())]);
			result.push_back(ring[indexize(localIndexTwo, ring.size())]);
			result.push_back(ring[indexize(localIndexTwo + 1, ring.size())]);
			localIndexTwo = indexize(localIndexTwo + 1, ring.size());
		}
	}

	if (!front) {
		std::reverse(result.begin(), result.end());
	}

	return result;
}

std::vector<vec3> assembleMesh(const std::vector<std::vector<vec3>>& rings, bool closeBottom, bool closeTop) {
	auto result = std::vector<vec3>{};

	if (closeBottom) {
		auto bottom = closeRing(rings[0], false);
		result.insert(result.end(), bottom.begin(), bottom.end());
	}

	for (int r = 0; r < rings.size()-1; ++r) {
		auto connection = connectRings(rings[r], rings[r+1]);
		result.insert(result.end(), connection.begin(), connection.end());
	}

	if (closeTop) {
		auto top = closeRing(rings[rings.size()-1], true);
		result.insert(result.end(), top.begin(), top.end());
	}

	return result;
}


std::tuple<std::vector<vec3>,std::vector<vec3>> assembleColoredMesh(
		const std::vector<std::vector<vec3>>& rings,
		const std::vector<std::vector<vec3>>& colors,
		bool closeBottom,
		bool closeTop) {
	auto positions = std::vector<vec3>{};
	auto triangleColors = std::vector<vec3>{};


	auto colorPicker = [](const std::vector<vec3>& ringColors){
		size_t index = rand() % ringColors.size();
		return ringColors[index];
	};

	if (closeBottom) {
		auto bottom = closeRing(rings[0], false);
		positions.insert(positions.end(), bottom.begin(), bottom.end());
		for (int i = 0; i < bottom.size(); i+=3) {
			auto c = colorPicker(colors[0]);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
		}
	}

	for (int r = 0; r < rings.size()-1; ++r) {
		auto connection = connectRings(rings[r], rings[r+1]);
		positions.insert(positions.end(), connection.begin(), connection.end());
		for (int i = 0; i < connection.size(); i+=3) {
			auto c = colorPicker(colors[r]);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
		}
	}

	if (closeTop) {
		auto top = closeRing(rings[rings.size()-1], true);
		positions.insert(positions.end(), top.begin(), top.end());
		for (int i = 0; i < top.size(); i+=3) {
			auto c = colorPicker(colors[rings.size()-1]);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
			triangleColors.push_back(c);
		}
	}

	return std::make_tuple(positions, triangleColors);
}


}

#endif /* SRC_MESH_ASSEMBLY_H_ */
