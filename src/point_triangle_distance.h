/*
 * point_triangle_distance.h
 *
 *  Created on: May 23, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_POINT_TRIANGLE_DISTANCE_H_
#define SRC_POINT_TRIANGLE_DISTANCE_H_

#include "miscellaneous.h"

// this is slightly modified version of Boost implementation:
//
// David Eberly, Geometric Tools, Redmond WA 98052
// Copyright (c) 1998-2020
// Distributed under the Boost Software License, Version 1.0.
// https://www.boost.org/LICENSE_1_0.txt
// https://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// Version: 4.0.2019.08.13

#pragma once

namespace ringasm {
class PointTriangleDistance {
public:
	struct Result {
		float distance, sqrDistance;
		float parameter[3];  // barycentric coordinates for triangle.v[3]
		glm::vec3 closest;
	};

	Result compute(const glm::vec3& point, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2) {
		glm::vec3 diff = point - v0;
		glm::vec3 edge0 = v1 - v0;
		glm::vec3 edge1 = v2 - v0;
		float a00 = glm::dot(edge0, edge0);
		float a01 = glm::dot(edge0, edge1);
		float a11 = glm::dot(edge1, edge1);
		float b0 = -glm::dot(diff, edge0);
		float b1 = -glm::dot(diff, edge1);

		float f00 = b0;
		float f10 = b0 + a00;
		float f01 = b0 + a01;

		glm::vec2 p0, p1, p;
		float dt1, h0, h1;

		// Compute the endpoints p0 and p1 of the segment.  The segment is
		// parameterized by L(z) = (1-z)*p0 + z*p1 for z in [0,1] and the
		// directional derivative of half the quadratic on the segment is
		// H(z) = glm::dot(p1-p0,gradient[Q](L(z))/2), where gradient[Q]/2 =
		// (F,G).  By design, F(L(z)) = 0 for cases (2), (4), (5), and
		// (6).  Cases (1) and (3) can correspond to no-intersection or
		// intersection of F = 0 with the triangle.
		if (f00 >= (float) 0) {
			if (f01 >= (float) 0) {
				// (1) p0 = (0,0), p1 = (0,1), H(z) = G(L(z))
				GetMinEdge02(a11, b1, p);
			} else {
				// (2) p0 = (0,t10), p1 = (t01,1-t01),
				// H(z) = (t11 - t10)*G(L(z))
				p0[0] = (float) 0;
				p0[1] = f00 / (f00 - f01);
				p1[0] = f01 / (f01 - f10);
				p1[1] = (float) 1 - p1[0];
				dt1 = p1[1] - p0[1];
				h0 = dt1 * (a11 * p0[1] + b1);
				if (h0 >= (float) 0) {
					GetMinEdge02(a11, b1, p);
				} else {
					h1 = dt1 * (a01 * p1[0] + a11 * p1[1] + b1);
					if (h1 <= (float) 0) {
						GetMinEdge12(a01, a11, b1, f10, f01, p);
					} else {
						GetMinInterior(p0, h0, p1, h1, p);
					}
				}
			}
		} else if (f01 <= (float) 0) {
			if (f10 <= (float) 0) {
				// (3) p0 = (1,0), p1 = (0,1), H(z) = G(L(z)) - F(L(z))
				GetMinEdge12(a01, a11, b1, f10, f01, p);
			} else {
				// (4) p0 = (t00,0), p1 = (t01,1-t01), H(z) = t11*G(L(z))
				p0[0] = f00 / (f00 - f10);
				p0[1] = (float) 0;
				p1[0] = f01 / (f01 - f10);
				p1[1] = (float) 1 - p1[0];
				h0 = p1[1] * (a01 * p0[0] + b1);
				if (h0 >= (float) 0) {
					p = p0;  // GetMinEdge01
				} else {
					h1 = p1[1] * (a01 * p1[0] + a11 * p1[1] + b1);
					if (h1 <= (float) 0) {
						GetMinEdge12(a01, a11, b1, f10, f01, p);
					} else {
						GetMinInterior(p0, h0, p1, h1, p);
					}
				}
			}
		} else if (f10 <= (float) 0) {
			// (5) p0 = (0,t10), p1 = (t01,1-t01),
			// H(z) = (t11 - t10)*G(L(z))
			p0[0] = (float) 0;
			p0[1] = f00 / (f00 - f01);
			p1[0] = f01 / (f01 - f10);
			p1[1] = (float) 1 - p1[0];
			dt1 = p1[1] - p0[1];
			h0 = dt1 * (a11 * p0[1] + b1);
			if (h0 >= (float) 0) {
				GetMinEdge02(a11, b1, p);
			} else {
				h1 = dt1 * (a01 * p1[0] + a11 * p1[1] + b1);
				if (h1 <= (float) 0) {
					GetMinEdge12(a01, a11, b1, f10, f01, p);
				} else {
					GetMinInterior(p0, h0, p1, h1, p);
				}
			}
		} else {
			// (6) p0 = (t00,0), p1 = (0,t11), H(z) = t11*G(L(z))
			p0[0] = f00 / (f00 - f10);
			p0[1] = (float) 0;
			p1[0] = (float) 0;
			p1[1] = f00 / (f00 - f01);
			h0 = p1[1] * (a01 * p0[0] + b1);
			if (h0 >= (float) 0) {
				p = p0;  // GetMinEdge01
			} else {
				h1 = p1[1] * (a11 * p1[1] + b1);
				if (h1 <= (float) 0) {
					GetMinEdge02(a11, b1, p);
				} else {
					GetMinInterior(p0, h0, p1, h1, p);
				}
			}
		}

		Result result;
		result.parameter[0] = (float) 1 - p[0] - p[1];
		result.parameter[1] = p[0];
		result.parameter[2] = p[1];
		result.closest = v0 + p[0] * edge0 + p[1] * edge1;
		diff = point - result.closest;
		result.sqrDistance = glm::dot(diff, diff);
		result.distance = std::sqrt(result.sqrDistance);
		return result;
	}

	// TODO: This is the previous implementation based on quadratic
	// minimization with constraints. It was replaced by the current
	// operator() that uses the conjugate gradient algorithm. I will
	// keep both in the upcoming GTL code, so the old code is restored
	// here for now.
	Result DistanceByQM(const glm::vec3& point, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2) {
		// The member result.sqrDistance is set each block of the nested
		// if-then-else statements. The remaining members are all set at
		// the end of the function.
		Result result;

		glm::vec3 diff = v0 - point;
		glm::vec3 edge0 = v1 - v0;
		glm::vec3 edge1 = v2 - v0;
		float a00 = glm::dot(edge0, edge0);
		float a01 = glm::dot(edge0, edge1);
		float a11 = glm::dot(edge1, edge1);
		float b0 = glm::dot(diff, edge0);
		float b1 = glm::dot(diff, edge1);
		float c = glm::dot(diff, diff);
		float det = std::max(a00 * a11 - a01 * a01, (float) 0);
		float s = a01 * b1 - a11 * b0;
		float t = a01 * b0 - a00 * b1;

		if (s + t <= det) {
			if (s < (float) 0) {
				if (t < (float) 0)  // region 4
					{
					if (b0 < (float) 0) {
						t = (float) 0;
						if (-b0 >= a00) {
							s = (float) 1;
							result.sqrDistance = a00 + (float) 2 * b0 + c;
						} else {
							s = -b0 / a00;
							result.sqrDistance = b0 * s + c;
						}
					} else {
						s = (float) 0;
						if (b1 >= (float) 0) {
							t = (float) 0;
							result.sqrDistance = c;
						} else if (-b1 >= a11) {
							t = (float) 1;
							result.sqrDistance = a11 + (float) 2 * b1 + c;
						} else {
							t = -b1 / a11;
							result.sqrDistance = b1 * t + c;
						}
					}
				} else  // region 3
				{
					s = (float) 0;
					if (b1 >= (float) 0) {
						t = (float) 0;
						result.sqrDistance = c;
					} else if (-b1 >= a11) {
						t = (float) 1;
						result.sqrDistance = a11 + (float) 2 * b1 + c;
					} else {
						t = -b1 / a11;
						result.sqrDistance = b1 * t + c;
					}
				}
			} else if (t < (float) 0)  // region 5
				{
				t = (float) 0;
				if (b0 >= (float) 0) {
					s = (float) 0;
					result.sqrDistance = c;
				} else if (-b0 >= a00) {
					s = (float) 1;
					result.sqrDistance = a00 + (float) 2 * b0 + c;
				} else {
					s = -b0 / a00;
					result.sqrDistance = b0 * s + c;
				}
			} else  // region 0
			{
				// minimum at interior point
				float invDet = ((float) 1) / det;
				s *= invDet;
				t *= invDet;
				result.sqrDistance = s * (a00 * s + a01 * t + (float) 2 * b0) + t * (a01 * s + a11 * t + (float) 2 * b1) + c;
			}
		} else {
			float tmp0, tmp1, numer, denom;

			if (s < (float) 0)  // region 2
				{
				tmp0 = a01 + b0;
				tmp1 = a11 + b1;
				if (tmp1 > tmp0) {
					numer = tmp1 - tmp0;
					denom = a00 - (float) 2 * a01 + a11;
					if (numer >= denom) {
						s = (float) 1;
						t = (float) 0;
						result.sqrDistance = a00 + (float) 2 * b0 + c;
					} else {
						s = numer / denom;
						t = (float) 1 - s;
						result.sqrDistance = s * (a00 * s + a01 * t + (float) 2 * b0) + t * (a01 * s + a11 * t + (float) 2 * b1) + c;
					}
				} else {
					s = (float) 0;
					if (tmp1 <= (float) 0) {
						t = (float) 1;
						result.sqrDistance = a11 + (float) 2 * b1 + c;
					} else if (b1 >= (float) 0) {
						t = (float) 0;
						result.sqrDistance = c;
					} else {
						t = -b1 / a11;
						result.sqrDistance = b1 * t + c;
					}
				}
			} else if (t < (float) 0)  // region 6
				{
				tmp0 = a01 + b1;
				tmp1 = a00 + b0;
				if (tmp1 > tmp0) {
					numer = tmp1 - tmp0;
					denom = a00 - (float) 2 * a01 + a11;
					if (numer >= denom) {
						t = (float) 1;
						s = (float) 0;
						result.sqrDistance = a11 + (float) 2 * b1 + c;
					} else {
						t = numer / denom;
						s = (float) 1 - t;
						result.sqrDistance = s * (a00 * s + a01 * t + (float) 2 * b0) + t * (a01 * s + a11 * t + (float) 2 * b1) + c;
					}
				} else {
					t = (float) 0;
					if (tmp1 <= (float) 0) {
						s = (float) 1;
						result.sqrDistance = a00 + (float) 2 * b0 + c;
					} else if (b0 >= (float) 0) {
						s = (float) 0;
						result.sqrDistance = c;
					} else {
						s = -b0 / a00;
						result.sqrDistance = b0 * s + c;
					}
				}
			} else  // region 1
			{
				numer = a11 + b1 - a01 - b0;
				if (numer <= (float) 0) {
					s = (float) 0;
					t = (float) 1;
					result.sqrDistance = a11 + (float) 2 * b1 + c;
				} else {
					denom = a00 - ((float) 2) * a01 + a11;
					if (numer >= denom) {
						s = (float) 1;
						t = (float) 0;
						result.sqrDistance = a00 + (float) 2 * b0 + c;
					} else {
						s = numer / denom;
						t = (float) 1 - s;
						result.sqrDistance = s * (a00 * s + a01 * t + (float) 2 * b0) + t * (a01 * s + a11 * t + (float) 2 * b1) + c;
					}
				}
			}
		}

		// Account for numerical round-off error.
		if (result.sqrDistance < (float) 0) {
			result.sqrDistance = (float) 0;
		}

		result.distance = sqrt(result.sqrDistance);
		result.closest = v0 + s * edge0 + t * edge1;
		result.parameter[1] = s;
		result.parameter[2] = t;
		result.parameter[0] = (float) 1 - s - t;
		return result;
	}

private:
	void GetMinEdge02(float const& a11, float const& b1, glm::vec2& p) {
		p[0] = (float) 0;
		if (b1 >= (float) 0) {
			p[1] = (float) 0;
		} else if (a11 + b1 <= (float) 0) {
			p[1] = (float) 1;
		} else {
			p[1] = -b1 / a11;
		}
	}

	inline void GetMinEdge12(float const& a01, float const& a11, float const& b1, float const& f10, float const& f01, glm::vec2& p) {
		float h0 = a01 + b1 - f10;
		if (h0 >= (float) 0) {
			p[1] = (float) 0;
		} else {
			float h1 = a11 + b1 - f01;
			if (h1 <= (float) 0) {
				p[1] = (float) 1;
			} else {
				p[1] = h0 / (h0 - h1);
			}
		}
		p[0] = (float) 1 - p[1];
	}

	inline void GetMinInterior(glm::vec2 const& p0, float const& h0, glm::vec2 const& p1, float const& h1, glm::vec2& p) {
		float z = h0 / (h0 - h1);
		p = ((float) 1 - z) * p0 + z * p1;
	}
};
}

#endif /* SRC_POINT_TRIANGLE_DISTANCE_H_ */
