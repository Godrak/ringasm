/*
 * assembler.h
 *
 *  Created on: 28 Nov 2021
 *      Author: pavel
 */

#ifndef SRC_ASSEMBLER_H_
#define SRC_ASSEMBLER_H_

#include "mesh_assembly.h"

namespace ringasm {

class Assembler {
public:
	std::vector<std::vector<vec3>> rings;
	std::vector<std::vector<vec3>> colors;

	Assembler(size_t ringSize, const std::vector<vec3> &initColors) {
		rings.push_back(createRingDefault(ringSize));
		colors.push_back(initColors);
	}

	Assembler(const std::vector<vec3> &ring, const std::vector<vec3> &initColors) {
		rings.push_back(ring);
		colors.push_back(initColors);
	}

	Assembler& scale(float factor) {
		scaleFromMassCenter(rings.back(), factor);
		return *this;
	}

	Assembler& translate(const vec3 &factor) {
		ringasm::translate(rings.back(), factor);
		return *this;
	}

	Assembler& translateRandom(const vec3 &min, const vec3 &max) {
		ringasm::translateRandom(rings.back(), min, max);
		return *this;
	}

	Assembler& resample(size_t newRingSize, bool shift) {
		auto newRing = resampleRingUniform(rings.back(), newRingSize, shift);
		rings.pop_back();
		rings.push_back(newRing);
		return *this;
	}

	Assembler& translateInNormal(float distance) {
		auto normal = glm::normalize(computeRingNormal(rings.back()));
		ringasm::translate(rings.back(), distance * normal);
		return *this;
	}

	Assembler& rotateToAlignNormal(const vec3 &newNormal) {
		ringasm::rotateInMassCenterToAlign(rings.back(), computeRingNormal(rings.back()), newNormal);
		return *this;
	}

	Assembler& setColors(const std::vector<vec3> newColors) {
		colors[colors.size() - 1] = newColors;
		return *this;
	}

	Assembler& addClone() {
		auto copy = rings.back();
		auto color = colors.back();
		rings.push_back(copy);
		colors.push_back(color);
		return *this;
	}

	Assembler& addCloneAfter(float distance) {
		addClone().translateInNormal(distance);
		return *this;
	}

	std::vector<vec3> getLast() {
		return rings.back();
	}

	Assembler& add(const std::vector<vec3> &ring) {
		rings.push_back(ring);
		auto color = colors.back();
		colors.push_back(color);
		return *this;
	}

	Assembler& add(size_t ringSize) {
		rings.push_back(createRingDefault(ringSize));
		auto color = colors.back();
		colors.push_back(color);
		return *this;
	}

	Assembler& add(const std::vector<vec3> &ring, const std::vector<vec3> &ringColors) {
		rings.push_back(ring);
		colors.push_back(ringColors);
		return *this;
	}

	std::vector<vec3> assemble(bool closeBottom, bool closeTop) {
		return ringasm::assembleMesh(rings, closeBottom, closeTop);
	}

	std::tuple<std::vector<vec3>, std::vector<vec3>> assembleColored(bool closeBottom, bool closeTop) {
		return ringasm::assembleColoredMesh(rings, colors, closeBottom, closeTop);
	}
};

}

#endif /* SRC_ASSEMBLER_H_ */
