/*
 * export.h
 *
 *  Created on: Jul 10, 2019
 *      Author: Pavel Mikus
 *		email: pavel.mikus@eyen.se
 */

#ifndef SRC_ELEMENTS_H_
#define SRC_ELEMENTS_H_

#include "vertex_transform.h"

#include <fstream>

namespace ringasm {

void exportToObj(const std::vector<vec3>& positions, const std::vector<vec3>& normals, std::string path) {
	std::ofstream file;
	file.open(path);
	for (auto p : positions) {
		file << "v " << p.x << " " << p.y << " " << p.z << std::endl;
	}
	for (auto n : normals) {
		file << "vn " << n.x << " " << n.y << " " << n.z << std::endl;
	}

	for (int index = 0; index < positions.size(); index += 3) {
		file << "f " << index + 1 << "//" << index + 1 << " " << index +2 << "//" << index +2
			<< " " << index + 3 << "//" <<index + 3 << std::endl;
	}

	file.close();
}

void exportColoredToObj(const std::vector<vec3>& positions, const std::vector<vec3>& normals,  const std::vector<vec3>& colors, std::string path) {
	std::ofstream file;
	file.open(path);
	for (int v = 0; v < positions.size(); ++v) {
		file << "v " << positions[v].x << " " << positions[v].y << " " << positions[v].z <<
				" " << colors[v].x << " " << colors[v].y << " " << colors[v].z << std::endl;
	}
	for (auto n : normals) {
		file << "vn " << n.x << " " << n.y << " " << n.z << std::endl;
	}

	for (int index = 0; index < positions.size(); index += 3) {
		file << "f " << index + 1 << "//" << index + 1 << " " << index +2 << "//" << index +2
			<< " " << index + 3 << "//" <<index + 3 << std::endl;
	}

	file.close();
}

}

#endif /* SRC_ELEMENTS_H_ */
