/*
 * vertex_transform.h
 *
 *  Created on: Jun 30, 2019
 *      Author: Pavel Mikus
 *		email: pavel.mikus@eyen.se
 */

#ifndef SRC_vec3_TRANSFORM_H
#define SRC_vec3_TRANSFORM_H

#include "miscellaneous.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include "glm/gtc/random.hpp"
#include "glm/gtx/transform.hpp"

namespace ringasm {

template<typename FUNCTOR>
void map(std::vector<vec3>& vertices, FUNCTOR functor) {
	for (auto& v : vertices) {
		functor(v);
	}
}

void translate(std::vector<vec3>& vertices, const vec3& factor) {
	map(vertices, [factor](vec3& v) {
		v += factor;
	});
}

void scale(std::vector<vec3>& vertices, const vec3& center, float factor) {
	map(vertices, [center, factor](vec3& v) {
		v -= center;
		v *= vec3(factor);
		v += center;
	});
}

void scaleFromMassCenter(std::vector<vec3>& vertices, float factor) {
	vec3 center = centerOfMass(vertices);
	scale(vertices, center, factor);
}

// x towards the center, y right, z up
void translateRandom(std::vector<vec3>& vertices, const vec3& min, const vec3& max) {
	vec3 center = centerOfMass(vertices);
	auto orig_pos_of_first = vertices[0];
	for (int index = 0; index < vertices.size(); ++index) {
		vec3 forward = glm::normalize(center - vertices[index]);
		auto pos_of_next = index + 1 < vertices.size() ? vertices[index + 1] : orig_pos_of_first;
		vec3 right = pos_of_next - vertices[index];
		right = glm::normalize(right);
		vec3 up = glm::normalize(glm::cross(right, forward));
		vec3 randOffset = glm::linearRand(min, max);

		vertices[index] += forward * vec3(randOffset.x);
		vertices[index] += right * vec3(randOffset.y);
		vertices[index] += up * vec3(randOffset.z);
	}
}

// counterclockwise in the axis direction
void rotate(std::vector<vec3>& vertices, const vec3& axis, const vec3& center, float degrees) {
	auto rotMatrix = glm::rotate(degrees, axis);
	map(vertices, [center, rotMatrix](vec3& v) {
		v -= center;
		glm::vec4 v4 = rotMatrix * glm::vec4(v, 1.0);
		v = vec3(v4.x, v4.y, v4.z);
		v += center;
	});
}

void rotateInMassCenter(std::vector<vec3>& vertices, const vec3& axis, float degrees) {
	vec3 center = centerOfMass(vertices);
	rotate(vertices, axis, center, degrees);
}

void rotateToAlign(std::vector<vec3>& vertices, const vec3& center, const vec3& vector, const vec3& targetVector) {
	auto rotMatrix = createVectorAlignMatrix(vector, targetVector);
	map(vertices, [center, rotMatrix](vec3& v) {
		v -= center;
		v= rotMatrix*v;
		v += center;
	});
}

void rotateInMassCenterToAlign(std::vector<vec3>& vertices, const vec3& vector, const vec3& targetVector) {
	vec3 center = centerOfMass(vertices);
	rotateToAlign(vertices, center, vector, targetVector);
}

}

#endif /* SRC_vec3_TRANSFORM_H_ */
