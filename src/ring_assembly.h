/*
 * ring_assembly.h
 *
 *  Created on: Jul 5, 2019
 *      Author: Pavel Mikus
 *		email: pavel.mikus@eyen.se
 */

#ifndef SRC_RING_ASSEMBLY_H_
#define SRC_RING_ASSEMBLY_H_

#include "miscellaneous.h"
#include "vertex_transform.h"
#include <cmath>

namespace ringasm {
//CCW rings are front facing!

//creates perfect circular ring with the given vertices count, rotated so that the first vertex defines direction from the centre to the up vector,
//normal defines the ring normal, and position defines the centre position
std::vector<vec3> createRing(size_t verticesCount, const vec3& position, const vec3& normal, const vec3& up) {
	std::vector<vec3> ring;
	for (int i = 0; i < verticesCount; i++) {
		float x = std::cos(2 * M_PI * float(i) / verticesCount + M_PI_2);
		float y = std::sin(2 * M_PI * float(i) / verticesCount + M_PI_2);
		ring.push_back(vec3 { x, y, 0 });
	}
	vec3 currentNormal = vec3(0, 0, 1);
	vec3 currentUp = vec3(0, 1, 0);
	auto upRotMatrix = createVectorAlignMatrix(currentUp, up);
	map(ring, [upRotMatrix](vec3& v) {
		v = upRotMatrix * v;
	});
	currentNormal = upRotMatrix * currentNormal;
	auto rotMatrix = createVectorAlignMatrix(currentNormal, normal);
	map(ring, [rotMatrix](vec3& v) {
		v = rotMatrix * v;
	});

	translate(ring, position);
	return ring;
}

std::vector<vec3> createRing(size_t verticesCount, const vec3& position, const vec3& normal) {
	return createRing(verticesCount, position, normal, vec3(0, 1, 0));
}


std::vector<vec3> createRingDefault(size_t ringSize) {
	return createRing(ringSize, vec3(0), vec3(0,0,1), vec3(0,1,0));
}

std::vector<vec3> resampleRingUniform(const std::vector<vec3>& ring, size_t verticesCount, bool shift) {
	assert(verticesCount > 0);
	assert(ring.size() > 0);
	float totalDistance = 0;
	std::vector<float> distances;
	std::vector<vec3> localRing;
	for (int index = 0; index < ring.size() - 1; ++index) {
		distances.push_back(totalDistance);
		localRing.push_back(ring[index]);
		totalDistance += glm::distance(ring[index], ring[index + 1]);
	}
	distances.push_back(totalDistance);
	localRing.push_back(ring[ring.size() - 1]);
	totalDistance += glm::distance(ring[0], ring[ring.size() - 1]);
	distances.push_back(totalDistance);
	localRing.push_back(ring[0]);

	float sampleDistance = totalDistance / verticesCount;
	float resampledDistance = shift ? sampleDistance / 2 : 0.0; // starting a little shifted, for nicer results
	int prevVertexIndex = 0;

	auto merger = [](const vec3& v1, const vec3& v2, float f) {
		return v1 * vec3(1 - f) + v2 * vec3(f);
	};
	std::vector<vec3> result;
	while (resampledDistance < totalDistance) {
		while (resampledDistance > distances[prevVertexIndex + 1]) {
			prevVertexIndex++;
		}
		result.push_back(
			merger(
				localRing[prevVertexIndex],
				localRing[prevVertexIndex + 1],
				(resampledDistance - distances[prevVertexIndex]) / (distances[prevVertexIndex + 1] - distances[prevVertexIndex])));
		resampledDistance += sampleDistance;
	}
	return result;
}

std::vector<std::vector<vec3>> createHermitRings(
	size_t layersCount,
	size_t layerVerticesCount,
	const vec3& startPos,
	const vec3& startTangent,
	const vec3& endPos,
	const vec3& endTangent)
{
	std::vector<std::vector<vec3>> result;
	for (int index = 0; index <= layersCount; ++index) {
		float t = float(index) / (layersCount);
		float t2 = t * t;
		float t3 = t * t * t;
		vec3 position = vec3(2 * t3 - 3 * t2 + 1) * startPos + vec3(t3 - 2 * t2 + t) * startTangent + vec3(-2 * t3 + 3 * t2) * endPos
			+ vec3(t3 - t2) * endTangent;
		vec3 normal = vec3(6 * t2 - 6 * t) * startPos + vec3(3 * t2 - 4 * t + 1) * startTangent + vec3(-6 * t2 + 6 * t) * endPos
			+ vec3(3 * t2 - 2 * t) * endTangent;
		auto ring = createRing(layerVerticesCount, position, normal);
		result.push_back(ring);
	}
	return result;
}

}

#endif /* SRC_RING_ASSEMBLY_H_ */
