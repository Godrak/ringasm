/*
 * miscellaneous.h
 *
 *  Created on: Jun 30, 2019
 *      Author: Pavel Mikus
 *		email: pavel.mikus@eyen.se
 */

#ifndef SRC_MISCELLANEOUS_H_
#define SRC_MISCELLANEOUS_H_

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include "glm/gtc/random.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtx/normal.hpp"

#include <iostream>
#include <vector>

namespace ringasm {

#define EPSILON 1e-4

using vec3 = glm::vec3;

template<typename I>
I indexize(I index, std::size_t size) {
	assert(size > 0);
	while (index < 0)
		index += size;
	return index % size;
}

template<typename ELEMENT>
ELEMENT next(const std::vector<ELEMENT>& vec, int index) {
	return vec[indexize(index + 1, vec.size())];
}

template<typename ELEMENT>
ELEMENT prev(const std::vector<ELEMENT>& vec, int index) {
	return vec[indexize(index - 1, vec.size())];
}

vec3 centerOfMass(const std::vector<vec3>& points) {
	assert(points.size() > 0);
	auto result = vec3 { };
	for (const auto& point : points) {
		result += point;
	}
	return result / vec3(points.size());
}

float minSequentialDistance(std::vector<vec3> vertices) {
	assert(vertices.size() > 1);
	float min = glm::distance(vertices[0], vertices[vertices.size() - 1]);
	for (size_t index = 0; index < vertices.size() - 1; ++index) {
		min = std::min(min, glm::distance(vertices[index], vertices[index + 1]));
	}
	return min;
}

float distanceToLine(const vec3& vertex, const vec3& start, const vec3& direction) {
	vec3 d = glm::normalize(direction);
	vec3 v = vertex - start;
	double t = glm::dot(v, d);
	vec3 pointOnLine = start + d * vec3(t);
	return glm::length(vertex - pointOnLine);
}

float computeCircumcircleRadius(float a, float b, float c) {
	return a * b * c / std::sqrt((a + b + c) * (a + b - c) * (a + c - b) * (b + c - a));
}

float computeArea(float a, float b, float c) {
	//Herons formula
	float halfPerimeter = (a + b + c) / 2;
	auto areaSquared = std::abs(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
	return std::sqrt(areaSquared);
}

float computeArea(vec3 a, vec3 b, vec3 c) {
	return computeArea(glm::distance(a, b), glm::distance(b, c), glm::distance(a, c));

}


vec3 computeTriangleNormal(const vec3& v0, const vec3& v1, const vec3& v2) {
	return cross(v0 - v1, v0 - v2);
}

vec3 computeRingNormal(const std::vector<vec3>& vertices) {
	assert(vertices.size() >= 3);
	auto com = centerOfMass(vertices);
	auto first = vertices[0] - com;
	vec3 normal { };
	for (size_t index = 1; index < vertices.size(); ++index) {
		auto next = vertices[index] - com;
		normal += glm::cross(first, next);
		first = next;
	}

	return normal;
}

bool equal(const vec3& val1, const vec3& val2) {
	return glm::length(val1 - val2) < glm::length(vec3(EPSILON));
}

bool equal(float val1, float val2) {
	return std::abs(val1 - val2) < EPSILON;
}

glm::mat3x3 createVectorAlignMatrix(const vec3& vector, const vec3& targetVector) {
	auto start = glm::normalize(vector);
	auto end = glm::normalize(targetVector);
	if (glm::length(start - end) < 1e-4f) {
		return glm::rotate(0.0f, vec3(0, 1, 0));
	}
	vec3 axis = glm::normalize(glm::cross(start, end));
	float cos = glm::dot(start, end);
	float angle = glm::acos(cos);
	auto mat = glm::rotate(angle, axis);
	return glm::mat3x3(mat);
}

void print(float f) {
	std::cout << f << std::endl;
}

void print(const vec3& v) {
	std::cout << v.x << " | " << v.y << " | " << v.z << std::endl;
}

}

#endif /* SRC_MISCELLANEOUS_H_ */
