/*
 * ringasm.cpp
 *
 *  Created on: Jun 1, 2019
 *      Author: Pavel Mikus
 *		mail: pavel.mikus@eyen.se
 */

#include <cmath>
#include <iostream>
#include <vector>

#include "acutest.h"

#include "miscellaneous.h"
#include "vertex_transform.h"
#include "ring_assembly.h"

using namespace ringasm;

namespace suppport {

float epsilon = 0.0001f;

bool compare(float val1, float val2) {
	return std::abs(val1 - val2) < epsilon;
}

bool inRange(float val, float min, float max) {
	return val >= min && val <= max;
}

bool compare(vec3 val1, vec3 val2) {
	return glm::length(val1 - val2) < glm::length(vec3(epsilon));
}

bool inRange(vec3 val, vec3 min, vec3 max) {
	return inRange(val.x, min.x, max.x) && inRange(val.y, min.y, max.y) && inRange(val.z, min.z, max.z);
}

}

void averagePositionTest(void) {
	auto v1 = vec3(3, 0, 0);
	auto v2 = vec3(0, 3, 0);
	auto v3 = vec3(0, 0, 3);

	auto pos = centerOfMass(std::vector<vec3> { v1, v2, v3 });

	TEST_CHECK_(suppport::compare(pos.x, 1), "X");
	TEST_CHECK_(suppport::compare(pos.y, 1), "Y");
	TEST_CHECK_(suppport::compare(pos.z, 1), "Z");
}

void distanceToLineTest(void) {
	auto v1 = vec3(1, 1, 1);

	auto dis1 = distanceToLine(v1, vec3(0), vec3(1));
	auto dis2 = distanceToLine(v1, vec3(0, 0, 1), vec3(1, 0, 0));

	TEST_CHECK(suppport::compare(dis1, 0));
	TEST_CHECK(suppport::compare(dis2, 1));
}

template<typename E>
bool vectorContainsSequence(std::vector<E> vec, std::vector<E> seq) {
	for (int seqStart = 0; seqStart < seq.size(); ++seqStart) {
		for (int index = 0; index < vec.size(); ++index) {
			if (vec[index] == seq[seqStart]) {
				bool found = true;
				for (int seqOffset = 0; seqOffset < seq.size(); ++seqOffset) {
					if (vec[indexize(index + seqOffset, vec.size())] != seq[indexize(seqOffset + seqStart, seq.size())]) {
						found = false;
					}
				}
				if (found) {
					return true;
				}
			}
		}
	}
	return false;
}

void translateTest(void) {
	std::vector<vec3> vertices = { vec3 { 0, 0, 0 }, vec3 { 1, 1, 1 }, };

	translate(vertices, vec3(1, 2, 3));
	TEST_CHECK_(suppport::compare(vertices[0], vec3(1, 2, 3)), "first vertex");
	TEST_CHECK_(suppport::compare(vertices[1], vec3(2, 3, 4)), "second vertex");
}

void scaleTest(void) {
	std::vector<vec3> vertices = { vec3 { -1, -1, 0 }, vec3 { -1, 1, 0 }, vec3 { 1, 1, 0 }, vec3 { 1, -1, 0 }, };

	scaleFromMassCenter(vertices, 2);
	TEST_CHECK_(suppport::compare(vertices[0], vec3(-2, -2, 0)), "0. vertex");
	TEST_CHECK_(suppport::compare(vertices[1], vec3(-2, 2, 0)), "1. vertex");
	TEST_CHECK_(suppport::compare(vertices[2], vec3(2, 2, 0)), "2. vertex");
	TEST_CHECK_(suppport::compare(vertices[3], vec3(2, -2, 0)), "3. vertex");
}

void translateRandomTest(void) {
	std::vector<vec3> vertices = { vec3 { -1, -1, 0 }, vec3 { -1, 1, 0 }, vec3 { 1, 1, 0 }, vec3 { 1, -1, 0 }, };

	translateRandom(vertices, vec3(0, 0, -1), vec3(0, 0, 1));

	TEST_CHECK_(suppport::inRange(vertices[0], vec3(-1, -1, -1), vec3(-1, -1, 1)), "0. vertex");
	TEST_CHECK_(suppport::inRange(vertices[1], vec3(-1, 1, -1), vec3(-1, 1, 1)), "1. vertex");
	TEST_CHECK_(suppport::inRange(vertices[2], vec3(1, 1, -1), vec3(1, 1, 1)), "2. vertex");
	TEST_CHECK_(suppport::inRange(vertices[3], vec3(1, -1, -1), vec3(1, -1, 1)), "3. vertex");
}

void rotateTest(void) {
	std::vector<vec3> vertices = { vec3 { -1, -1, 0 }, vec3 { -1, 1, 0 }, vec3 { 1, 1, 0 }, vec3 { 1, -1, 0 }, };

	auto result = vertices;
	rotateInMassCenter(result, vec3(0, 0, 1), -M_PI_2);

	TEST_CHECK_(suppport::compare(result[0], vertices[1]), "0. vertex");
	TEST_CHECK_(suppport::compare(result[1], vertices[2]), "1. vertex");
	TEST_CHECK_(suppport::compare(result[2], vertices[3]), "2. vertex");
	TEST_CHECK_(suppport::compare(result[3], vertices[0]), "3. vertex");
}

void alignVectorsTest(void) {
	vec3 start = glm::normalize(vec3(1, 1, 1));
	vec3 end = { 1, 0, 0 };
	auto rotMatrix = createVectorAlignMatrix(start, end);
	auto res = rotMatrix * start;
	TEST_CHECK_(suppport::compare(res, end), "vector align");
}

void createRingTest(void) {
	auto ring = createRing(4, vec3(0, 0, 0), vec3(0, 0, 1));
	TEST_CHECK_(suppport::compare(ring[0], glm::normalize(vec3(0, 1, 0))), "0. vertex");
	TEST_CHECK_(suppport::compare(ring[1], glm::normalize(vec3(-1, 0, 0))), "1. vertex");
	TEST_CHECK_(suppport::compare(ring[2], glm::normalize(vec3(0, -1, 0))), "2. vertex");
	TEST_CHECK_(suppport::compare(ring[3], glm::normalize(vec3(1, 0, 0))), "3. vertex");
}

void resampleRingUniformTest(void) {
	auto ring = createRing(4, vec3(0, 0, 0), vec3(-1, 0, 0));
	auto resampled = resampleRingUniform(ring, 4, true);
	auto merger = [](const vec3& v1, const vec3& v2, float f) {
		return v1 * vec3(1 - f) + v2 * vec3(f);
	};
	TEST_CHECK_(suppport::compare(resampled[0], merger(ring[0], ring[1], 0.5f)), "0. vertex");
	TEST_CHECK_(suppport::compare(resampled[1], merger(ring[1], ring[2], 0.5f)), "1. vertex");
	TEST_CHECK_(suppport::compare(resampled[2], merger(ring[2], ring[3], 0.5f)), "2. vertex");
	TEST_CHECK_(suppport::compare(resampled[3], merger(ring[3], ring[0], 0.5f)), "3. vertex");
}


TEST_LIST = {
	{	"averagePositionTest", averagePositionTest},
	{	"distanceToLineTest", distanceToLineTest},
	{	"translateTest", translateTest},
	{	"scaleTest", scaleTest},
	{	"translateRandomTest", translateRandomTest},
	{	"rotateTest",rotateTest},
	{	"alignVectorsTest",alignVectorsTest},
	{	"createRingTest",createRingTest},
	{	"resampleRingUniformTest",resampleRingUniformTest},
	{	0}
};

