//just a testing file

#include "src/export.h"
#include "src/assembler.h"

using vec3 = ringasm::vec3;

void makeCube() {
	auto cube = ringasm::Assembler(4, { vec3(1, 0, 0) })
			.scale(1.0 / sqrt(2.0))
			.addCloneAfter(1)
			.assemble(true, true);

	auto normals = ringasm::computeNormals(cube);

	ringasm::exportToObj(cube, normals, "cube.obj");
}

void makeRedCube() {
	auto cube = ringasm::Assembler(4, { vec3(1, 0, 0) })
			.addCloneAfter(1)
			.assembleColored(true, true);

	auto normals = ringasm::computeNormals(std::get<0>(cube));

	ringasm::exportColoredToObj(std::get<0>(cube), normals, std::get<1>(cube),
			"redcube.obj");
}

void makeRedTree() {
	auto genShades = [](const vec3 &mean, size_t count) {
		std::vector<vec3> result { };
		for (int i = 0; i < count; ++i) {
			result.push_back(
					glm::clamp(glm::gaussRand(mean, vec3(0.1, 0.10, 0.10)),
							vec3(0), vec3(1))
									);
		}
		return result;
	};

	auto brownShades = genShades(vec3(0.58, 0.20, 0.20), 3);
	auto redShades = genShades(vec3(1.0, 0.0, 0.0), 5);

	auto tree = ringasm::Assembler(6, brownShades)
			.addCloneAfter(1).translateRandom(vec3(0.1), vec3(0.1))
			.addCloneAfter(1.5).translateRandom(vec3(0.1), vec3(0.1))
			.setColors(redShades)
			.addCloneAfter(0.6).resample(5, true).scale(3.0).translateRandom(
			vec3(0.1), vec3(0.1))
			.addCloneAfter(1.0).resample(6, false).scale(1.8).translateRandom(
			vec3(0.1), vec3(0.1))
			.addCloneAfter(1.4).resample(6, true).scale(1.2).translateRandom(
			vec3(0.1), vec3(0.1))
			.addCloneAfter(1.0).resample(6, true).scale(0.95).translateRandom(
			vec3(0.1), vec3(0.1))
			.addCloneAfter(1.0).resample(5, false).scale(0.8).translateRandom(
			vec3(0.1), vec3(0.1))
			.addCloneAfter(1.0).resample(4, false).scale(0.5).translateRandom(
			vec3(0.1), vec3(0.1))
			.assembleColored(true, true);

	auto normals = ringasm::computeNormals(std::get<0>(tree));

	ringasm::exportColoredToObj(std::get<0>(tree), normals, std::get<1>(tree),
			"redtree.obj");
}

void makeBall(void) {
	auto ball = ringasm::Assembler(3,
			{ glm::vec3(1, 1, 0), glm::vec3(0.2, 1, 0), glm::vec3(
					0.8, 1, 0) })
			.translate( { 0, 0, -0.8 }).scale(sin(0.2) * M_PI / 2)
			.add(4).translate( { 0, 0, -0.6 }).scale(sin(0.4) * M_PI / 2)
			.add(6).translate( { 0, 0, -0.4 }).scale(sin(0.6) * M_PI / 2)
			.add(8).translate( { 0, 0, -0.2 }).scale(sin(0.8) * M_PI / 2)
			.add(10).translate( { 0, 0, 0.0 }).scale(sin(0.8) * M_PI / 2)
			.add(8).translate( { 0, 0, 0.2 }).scale(sin(0.8) * M_PI / 2)
			.add(6).translate( { 0, 0, 0.4 }).scale(sin(0.6) * M_PI / 2)
			.add(4).translate( { 0, 0, 0.6 }).scale(sin(0.4) * M_PI / 2)
			.add(3).translate( { 0, 0, 0.8 }).scale(sin(0.2) * M_PI / 2)
			.assembleColored(true, true);

	ringasm::exportToObj(std::get<0>(ball),
			ringasm::computeNormals(std::get<0>(ball)), "ball.obj");
}

int main() {
	makeCube();
	makeRedCube();
	makeRedTree();
	makeBall();
}
